# DeepCaster

recurrent plot to 3D

## Motivation

## Dataset
### create 1000 pairs of Lorenz attractors and recurrence plots
[create_dataset.py](src/create_dataset.py)

```
python create_dataset.py
```
produces:
- [cubes.npy](https://www.dropbox.com/s/g7yi5pcf0jbwkil/cubes.npy?dl=0)
- [maps.npy](https://www.dropbox.com/s/33o52cgtmt3x6b9/maps.npy?dl=0)
