import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.ndimage import zoom
from scipy.spatial import distance
import random
from PIL import Image
import os
from scipy.spatial import distance


def remove_dup_coordinates(x,y,z):
    coords=dict()
    for i in range(0,len(x)):
        coords[ (x[i],y[i],z[i]) ]=1
    
    x_unique,y_unique,z_unique = list(),list(),list()
    
    for point in coords.keys():
        x_unique.append(point[0])
        y_unique.append(point[1])
        z_unique.append(point[2])
    
    return(np.array(x_unique),np.array(y_unique),np.array(z_unique))


'''def walk2cube( xs, ys, zs,LIMIT):

    x = np.digitize( xs, np.linspace(0,max(xs),LIMIT),right=True) 
    y = np.digitize( ys, np.linspace(0,max(ys),LIMIT),right=True) 
    z = np.digitize( zs, np.linspace(0,max(zs),LIMIT),right=True) 

    x,y,z = remove_dup_coordinates(x,y,z)
    #print(x)
    hic = coords2hic(x,y,z,LIMIT)

    cube = np.zeros((LIMIT,LIMIT,LIMIT),dtype='int16')
    for i in range(0,len(x)):
        cube[x[i],y[i],z[i]]=1

    points=list()
    for i in range(len(x)):
        points.append(np.array([x[i],y[i],z[i]]))
    points = np.asarray(points)

    yield cube, hic, np.array(x)/LIMIT, np.array(y)/LIMIT, np.array(z)/LIMIT, points'''
 

def walk2cube( xs, ys, zs,LIMIT):

    x = np.digitize( xs, np.linspace(0,max(xs),LIMIT),right=True) 
    y = np.digitize( ys, np.linspace(0,max(ys),LIMIT),right=True) 
    z = np.digitize( zs, np.linspace(0,max(zs),LIMIT),right=True) 

    x,y,z = remove_dup_coordinates(x,y,z)
    #print(x)
    hic = coords2hic(x,y,z,LIMIT)
    
    #cube = np.zeros((LIMIT,LIMIT,LIMIT,2),dtype='int16')
    cube = np.zeros((LIMIT,LIMIT,LIMIT,1),dtype='int16')
    #cube[:,:,:,0]=1 # EMPTY SAPCE
    #cube[:,:,:,1]=0 # CHR
    for i in range(0,len(x)):
        #cube[x[i],y[i],z[i],1]=1
        cube[x[i],y[i],z[i],0]=1

    points=list()
    for i in range(len(x)):
        points.append(np.array([x[i],y[i],z[i]]))
    points = np.asarray(points)

    yield cube, hic, np.array(x)/LIMIT, np.array(y)/LIMIT, np.array(z)/LIMIT, points

def coords2hic(xs,ys,zs,LIMIT):

    hic=np.empty((len(xs),len(xs)))
    
    if np.isnan(xs).any():
        print('ERROR')
        exit(1)
        #return hic
    
    for i in range(0, len(xs)):
        for j in range(0, len(xs)):
            d = distance.euclidean( [xs[i],ys[i],zs[i] ] , [ xs[j],ys[j],zs[j] ] )
            hic[i,j]=d
    
    #### Euclidean
    hic=normalize_0_1(hic)
    # make distances similarities
    hic = 1 - hic
    im = Image.fromarray(hic).resize((LIMIT,LIMIT))
    hic = np.asarray(im).reshape((LIMIT,LIMIT))
    print(np.mean(hic))
    return(hic)

def normalize_0_1(in_image):
    image = np.copy(in_image)
    if np.min(image) >= 0:
        image-=np.min(image)
    else:
        image+=abs(np.min(image))
    image = image/np.max(image)
    return image



def random_walk(NUM_STEPS):

    R = (np.random.rand(NUM_STEPS)*6).astype("int")
    x = np.zeros(NUM_STEPS)
    y = np.zeros(NUM_STEPS)
    z = np.zeros(NUM_STEPS)
    x[ R==0 ] = -1; x[ R==1 ] = 1
    y[ R==2 ] = -1; y[ R==3 ] = 1
    z[ R==4 ] = 1; z[ R==5 ] = -1
    x = np.cumsum(x)
    y = np.cumsum(y)
    z = np.cumsum(z)
    x+=abs(np.min(x))
    y+=abs(np.min(y))
    z+=abs(np.min(z))
    #x/=np.max(x)
    #y/=np.max(y)
    #z/=np.max(z)
    
    return(x,y,z)


#stats_output_file='stats.txt'
NUM_STEPS=100
LIMIT=100
DATASETSIZE=100
cubes=list()
hic_maps=list()
x_1d = list()
y_1d = list()
z_1d = list()
x_y_z = list()
counter=0
TERMINATE=False

#f = open(stats_output_file,'w')
while TERMINATE==False:
 
    xs, ys, zs= random_walk(NUM_STEPS)
 
    if np.isnan(xs).any():
        print('pppp')
        continue

    for cube, hic, x, y, z, points in walk2cube( xs, ys, zs, LIMIT):
        
        if counter >= DATASETSIZE:
            TERMINATE=True
            break   
            
        print(counter)
    
        counter+=1
        cubes.append(cube)
        hic_maps.append(hic)
        x_1d.append(x)
        y_1d.append(y)
        z_1d.append(z)
        x_y_z.append(points)
        
        matplotlib.image.imsave('hic_'+str(counter)+'.png', hic)

        xr,yr,zr = np.nonzero(cube[:,:,:,0])
        ax = plt.figure().add_subplot(projection='3d')
        ax.plot(xr,yr,zr, lw=0,  marker='o')
        ax.set_xlabel("X Axis")
        ax.set_ylabel("Y Axis")
        ax.set_zlabel("Z Axis")
        ax.set_title("CUBE")
        plt.savefig('cube_'+str(counter)+'.png')            


x_y_z= np.asarray(x_y_z)
cubes = np.asarray(cubes)
x_1d = np.asarray(x_1d)
y_1d = np.asarray(y_1d)
z_1d = np.asarray(z_1d)
hic_maps = np.asarray(hic_maps)

print(hic_maps.shape)
print(x_y_z.shape)
print(cubes.shape)
print(x_1d.shape)
print(y_1d.shape)
print(z_1d.shape)

np.save('cubes.npy', cubes)
np.save('maps.npy', hic_maps)
np.save('x_y_z.npy', x_y_z)
np.save('x.npy', x_1d)
np.save('y.npy', y_1d)
np.save('z.npy', z_1d)
#f.close()


import tarfile

with tarfile.open("dataset.tar.gz","w:gz") as tar:
    for file in ["maps.npy","x_y_z.npy","x.npy","y.npy","z.npy", 'cubes.npy']:
        tar.add(os.path.basename(file))
