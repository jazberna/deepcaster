import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.ndimage import zoom
from scipy.spatial import distance
import random
from PIL import Image
import os
from scipy.spatial import distance

def lorenz(x, y, z, s=10, r=28, b=2.667):
    """
    Given:
       x, y, z: a point of interest in three dimensional space
       s, r, b: parameters defining the lorenz attractor
    Returns:
       x_dot, y_dot, z_dot: values of the lorenz attractor's partial
           derivatives at the point x, y, z
    """
    x_dot = s*(y - x)
    y_dot = r*x - y - x*z
    z_dot = x*y - b*z
    return x_dot, y_dot, z_dot


def create_lorenz(s=100,r=50,b=100,num_steps=10000,start_x=0.,start_y=1.,start_z=1.05):
    dt = 0.01
   

    # Need one more for the initial values
    xs = np.empty(num_steps + 1)
    ys = np.empty(num_steps + 1)
    zs = np.empty(num_steps + 1)

    # Set initial values
    xs[0], ys[0], zs[0] = (start_x,start_y,start_z)
    #print(xs[0], ys[0], zs[0])

    # Step through "time", calculating the partial derivatives at the current point
    # and using them to estimate the next point
    for i in range(num_steps):
        x_dot, y_dot, z_dot = lorenz(xs[i], ys[i], zs[i], s,r,b)
        xs[i + 1] = xs[i] + (x_dot * dt)
        ys[i + 1] = ys[i] + (y_dot * dt)
        zs[i + 1] = zs[i] + (z_dot * dt)
    #print(len(xs),len(ys),len(zs))
    return([xs,ys,zs])

def remove_dup_coordinates(x,y,z):
    coords=dict()
    for i in range(0,len(x)):
        coords[ (x[i],y[i],z[i]) ]=1
    
    x_unique,y_unique,z_unique = list(),list(),list()
    
    for point in coords.keys():
        x_unique.append(point[0])
        y_unique.append(point[1])
        z_unique.append(point[2])
    
    return(np.array(x_unique),np.array(y_unique),np.array(z_unique))


def plot_lorenz( xs, ys, zs):

    ax = plt.figure().add_subplot(projection='3d')
    
    ax.plot(xs, ys, zs, lw=0.5)
    ax.set_xlabel("X Axis")
    ax.set_ylabel("Y Axis")
    ax.set_zlabel("Z Axis")
    ax.set_title("Lorenz Attractor")
    
    plt.show()

def lorent2cube2( xs, ys, zs,LIMIT=100):

    if np.min(xs) < 0:
        xs-=np.min(xs)
    
    if np.min(ys) < 0:
        ys-=np.min(ys)
    
    if np.min(zs) < 0:
        zs-=np.min(zs)


    x = np.digitize( xs, np.linspace(0,max(xs),LIMIT),right=True) 
    y = np.digitize( ys, np.linspace(0,max(ys),LIMIT),right=True) 
    z = np.digitize( zs, np.linspace(0,max(zs),LIMIT),right=True) 

    
    x,y,z = remove_dup_coordinates(x,y,z)

    hic = coords2hic(x,y,z,LIMIT)
    cube = np.zeros((LIMIT,LIMIT,LIMIT),dtype='int16')
    for i in range(0,len(x)):
       cube[x[i],y[i],z[i]]=1
    
    yield cube, hic, x/( LIMIT - 1 ), y/( LIMIT - 1 ), z/( LIMIT - 1 )
    
    '''rotations = rotate_cube(cube)
    for rotation in rotations:
        rotated_cube = np.zeros((LIMIT,LIMIT,LIMIT),dtype='int16')
        x,y,z = rotation.nonzero()
        x,y,z = remove_dup_coordinates(x,y,z)
        rotated_hic = coords2hic(x,y,z,LIMIT)
        for i in range(0,len(x)):
            rotated_cube[x[i],y[i],z[i]]=1
        #print(x)
        yield rotated_cube, rotated_hic, x/( LIMIT - 1 ), y/( LIMIT - 1 ), z/( LIMIT - 1 )'''

 

        
def lorent2cube( xs, ys, zs,LIMIT=100):

    if np.min(xs) < 0:
        xs-=np.min(xs)
    
    if np.min(ys) < 0:
        ys-=np.min(ys)
    
    if np.min(zs) < 0:
        zs-=np.min(zs)
    hic = coords2hic(xs,ys,zs,LIMIT)
     
    #print(min(xs),max(xs))
    #print(min(ys),max(ys))
    #print(min(zs),max(zs))
    #l = int(max([max(xs),max(ys),max(zs)]))
    
    #print(len(xs))
    x = np.digitize( xs, np.linspace(0,max(xs),LIMIT),right=True) 
    y = np.digitize( ys, np.linspace(0,max(ys),LIMIT),right=True) 
    z = np.digitize( zs, np.linspace(0,max(zs),LIMIT),right=True) 
    #print(np.min(x),np.max(x),len(x))
    #if np.min(x) == 0:
    #    print(xs)
    #    print('0000000000000000000000000000')
    #    #exit(0)
    #print('********')
    
    x,y,z = remove_dup_coordinates(x,y,z)
    hic_dig = coords2hic(x,y,z,LIMIT)
    #x=x[0:LIMIT-1]
    #y=y[0:LIMIT-1]
    #z=z[0:LIMIT-1]
    #print(len(x))
    #print(len(y))
    #print(len(z))
    #print((x))
    #print((y))
    #print((z))  

    
    #print(z[100:200])
    #print('------')
    #print(min(x),max(x))
    #print(min(y),max(y))
    #print(min(z),max(z))

    
    #print('++++++')
    
    # CREATE HIC MAP HERE!!!!!!! #####################
   
    #print(np.mean(hic))
    #print(hic.shape)
    #hic=False
    cube = np.zeros((LIMIT,LIMIT,LIMIT),dtype='int16')
    #cube = np.full((LIMIT,LIMIT,LIMIT),-1,dtype='int16')
    
    for i in range(0,len(x)):
       cube[x[i],y[i],z[i]]=1
    
    
    x_normalized = x / ( LIMIT - 1 )
    y_normalized = y / ( LIMIT - 1 )
    z_normalized = z / ( LIMIT - 1 )
    
    #print(len(x),len(y),len(z),np.min(x),np.max(x),np.min(y),np.max(y),np.min(z),np.max(z))
    #print(len(x_normalized),len(y_normalized),len(z_normalized),np.min(x_normalized),np.min(y_normalized),np.min(z_normalized),np.max(x_normalized),np.max(y_normalized),np.max(z_normalized))
    
    yield cube, hic, hic_dig, x_normalized , y_normalized  , z_normalized
    '''rotations = rotate_cube(cube)
    for rotation in rotations:
        rotated_cube = np.zeros((LIMIT,LIMIT,LIMIT),dtype='int16')
        x,y,z = rotation.nonzero()
        x,y,z = remove_dup_coordinates(x,y,z)
        rotated_hic = coords2hic(x,y,z,LIMIT,REDUCTION)
        for i in range(0,len(x)):
            rotated_cube[x[i],y[i],z[i]]=1
            #print(a)
            #print(b)
            #print(c)
        yield rotated_cube, rotated_hic'''
    
    #return(cube, hic)

#def cube2hic(cube,xs,yx,zs,LIMIT):
def coords2hic(xs,ys,zs,LIMIT):
    #print(xs)
    #print(ys)
    #print(zs)
    
    #xs = np.digitize( xs, np.linspace(0,max(xs),int(LIMIT/REDUCTION)),right=True)
    #ys = np.digitize( ys, np.linspace(0,max(ys),int(LIMIT/REDUCTION)),right=True)
    #zs = np.digitize( zs, np.linspace(0,max(zs),int(LIMIT/REDUCTION)),right=True)
    #print(len(xs))
    #print(len(ys))
    #print(len(zs))
    #xs,ys,zs = remove_dup_coordinates(xs,ys,zs)
    #print('__________')
    #print(len(xs))
    #print(len(ys))
    #print(len(zs))
    hic=np.empty((len(xs),len(xs)))
    
    if np.isnan(xs).any():
        return hic
    
    for i in range(0, len(xs)):
        for j in range(0, len(xs)):
            #print('******')
            print([xs[i],ys[i],zs[i] ] , [ xs[j],ys[j],zs[j] ])
            d = distance.euclidean( [xs[i],ys[i],zs[i] ] , [ xs[j],ys[j],zs[j] ]  )
            #s = 1- distance.cosine([xs[i],ys[i],zs[i] ] , [ xs[j],ys[j],zs[j] ] )
            print(d)
            #if s <=0.7: 
            #    s=0
            hic[i,j]=d
    #hic = (hic-np.mean(hic))/np.std(hic)
    hic=normalize_0_1(hic)
    # make distances similarities
    hic = 1 - hic
    #im = Image.fromarray(hic).resize((LIMIT,LIMIT))
    #hic = np.asarray(im).reshape((LIMIT,LIMIT))
    #print(hic.shape)
    #print('DONE..................................')

    #if hic.shape[0] > LIMIT:
    #    hic = hic[:LIMIT,:LIMIT]
               #plt.imshow(hic, cmap='gray')
    #           #plt.show()
    ###### hic = np.transpose(hic)
    return(hic)

def normalize_0_1(in_image):
    image = np.copy(in_image)
    if np.min(image) >= 0:
        image-=np.min(image)
    else:
        image+=abs(np.min(image))
    image = image/np.max(image)
    return image

def rotate_cube(cube):
    rotations = list()
    for plane in [(0,1),(1,2),(0,2)]:
        for i in range(0,4):
            rotations.append(np.rot90(cube,k=i,axes=plane))
    return rotations
    
'''def rotate_cube(cube):
    rotations = list()
    for i in range(0,5):
        for plane in [(0,1),(1,2),(0,2)]:
            rotations.append(np.rot90(cube,k=i,axes=plane))
    return rotations'''


stats_output_file='stats.txt'
NUM_STEPS=99
LIMIT=100
DATASETSIZE=1000
cubes=list()
hic_maps=list()
x_1d = list()
y_1d = list()
z_1d = list()
counter=0
TERMINATE=False

f = open(stats_output_file,'w')
while TERMINATE==False:
 
    #s = random.choice(np.linspace(10,100, 10))
    #r = random.choice(np.linspace(28,50, 10))
    #b = random.choice(np.linspace(2.667,100, 10))
    
    s = random.choice(np.linspace(90,100, 1000))
    r = random.choice(np.linspace(40,50, 1000))
    b = random.choice(np.linspace(90,100,1000))
    
    
    start_x = random.choice(range(1,100))
    start_y = random.choice(range(1,100))
    start_z = random.choice(range(1,100))
    
    xs, ys, zs= create_lorenz(s,r,b,NUM_STEPS,start_x,start_y,start_z)
    if np.isnan(xs).any():
        continue
    
    for cube, hic , x, y , z   in lorent2cube2( xs, ys, zs, LIMIT):
        if counter >= DATASETSIZE:
            TERMINATE=True
            break   
        
        #if len(x) in range(NUM_STEPS,LIMIT*2):
        if len(x) == LIMIT:
        #if np.mean(hic) >= MIN_INTENSITY and len(x) == LIMIT:
            print(counter)
            counter+=1
            #cubes.append(cube)
            hic_maps.append(hic)
            x_1d.append(x)
            y_1d.append(y)
            z_1d.append(z)
            
            
            #matplotlib.image.imsave('hic_'+str(counter)+'_dig.png', hic_dig)
            matplotlib.image.imsave('hic_'+str(counter)+'.png', hic)
            # Plot
            #x,y,z=cube.nonzero()
            ax = plt.figure().add_subplot(projection='3d')
            ax.plot(x, y, z, lw=0.5)
            ax.set_xlabel("X Axis")
            ax.set_ylabel("Y Axis")
            ax.set_zlabel("Z Axis")
            ax.set_title("Lorenz Attractor")
            plt.savefig('cube_'+str(counter)+'.png')
            
            stats = "counter:{} s:{} r:{} b:{} start_x:{} start_y:{} start_z:{}\n".format(counter,s,r,b,start_x,start_y,start_z)
            f.write(stats)
            f.flush()
#np.save('cubes.npy', cubes)
np.save('maps.npy', hic_maps)
np.save('x.npy', x_1d)
np.save('y.npy', y_1d)
np.save('z.npy', z_1d)
f.close()
